import GlobalStyles from './styles/global';

export function App() {
  return (
    <div>
      <h1>Início</h1>
      <GlobalStyles />
    </div>
  );
}
